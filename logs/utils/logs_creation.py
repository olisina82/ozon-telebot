import os
from datetime import datetime


def write_log_error(error_message: str):
    """
        Записывает сообщение об ошибке в лог-файл.

        Args:
            error_message (str): Сообщение об ошибке.

        Returns:
            None
        """
    error_record = f'{datetime.now()} {error_message}\n'
    project_root = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
    log_file_path = os.path.join(project_root, 'logs', 'error_log.txt')
    with open(log_file_path, 'a', encoding='utf-8') as log_file:
        log_file.write(error_record)

def log_error_decorator(error_text: str):
    """
        Декоратор для логирования ошибок.

        Args:
            error_text (str): Текст, который будет добавлен к сообщению об ошибке.

        Returns:
            Callable: Декорированная функция.
        """
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                result = func(*args, **kwargs)
                return result
            except Exception as exc:
                if args.from_user.id is not None:
                    exc_record = f'ID пользователя: {args.from_user.id}{error_text} {str(exc)}'
                else:
                    exc_record = f'ID пользователя: - {error_text} {str(exc)}'
                write_log_error(exc_record)
                return None
        return wrapper
    return decorator
