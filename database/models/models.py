from peewee import Model, DateTimeField, BigIntegerField, CharField, SqliteDatabase, ForeignKeyField
from datetime import datetime

db = SqliteDatabase('tg_cactus.db')


class BaseModel(Model):
    """"
    Базовая модель для всех моделей базы данных.

    :param created_at: Поле для хранения даты и времени создания записи.
    """
    created_at: datetime = DateTimeField(default=datetime.now().replace(microsecond=0))

    class Meta:
        database = db


class Query(BaseModel):
    """
        Модель для хранения запросов пользователя.

        :param query_id: Уникальный идентификатор запроса.
        :param user_id: Идентификатор пользователя, связанный с запросом.
        :param query_text: Текст запроса.

        """
    query_id: int = BigIntegerField(primary_key=True)
    user_id: int = BigIntegerField()
    query_text: str = CharField()


class Response(BaseModel):
    """
        Модель для хранения ответов на запросы.

        :param response_id: Уникальный идентификатор ответа.
        :param query: Связь с моделью Query, указывающая на запрос, к которому относится ответ.
        :param response_text: Текст ответа.

        """
    response_id: int = BigIntegerField(primary_key=True)
    query: Query = ForeignKeyField(Query, backref='responses')
    response_text: str = CharField()
