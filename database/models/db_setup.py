from peewee import SqliteDatabase
from models import Query, Response

db = SqliteDatabase('tg_cactus')

db.connect()
db.create_tables([Query, Response], safe=True)
