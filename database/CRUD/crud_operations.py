from ..models.models import Query, Response, db
from datetime import datetime
from logs.utils.logs_creation import write_log_error
from typing import Tuple, List, Optional


def create_query_response(user_id: int, query_text: str, response_text: list) \
        -> tuple[Query, Response] | tuple[None, None]:
    """
    Создает запись запроса и ответа в базе данных.

    :param user_id: ID пользователя
    :param query_text: Текст запроса
    :param response_text: Список текстов ответа
    :return: Кортеж с созданным объектом запроса и объектом ответа, или None в случае ошибки

    :exception Если не удается создать запись запроса или ответа, будет сгенерировано исключение.
    """
    try:
        with db.atomic():
            query = Query.create(
                user_id=user_id,
                query_text=query_text
            )
            response = Response.create(
                query=query,
                response_text=response_text
            )
        return query, response

    except Exception as exc:
        exc_record = f'ID пользователя: {user_id} Ошибка при работе с БД  {str(exc)}'
        write_log_error(exc_record)
        return None, None


def read_query_responses() -> List:
    """
        Читает последние 10 записей запросов и ответов из базы данных.

        :return: Список объектов запросов
        :exception Если не удается прочитать запись запроса или ответа, будет сгенерировано исключение.
        """
    try:
        with db.atomic():
            # Выбираем последние 10 записей с сортировкой по убыванию query_id
            queries_responses = (Query
                                 .select()
                                 .join(Response)
                                 .order_by(Query.query_id.desc())
                                 .limit(10))

        return queries_responses
    except Exception as exc:
        exc_record = f'{datetime.now()} Ошибка при чтении последних 10 записей из БД: {str(exc)}'
        write_log_error(exc_record)
        return []
