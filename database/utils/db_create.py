from database.models.models import Query, Response, db


def create_database(database=db):
    """
        Создает базу данных и таблицы для запросов и ответов.

        Args:
            database (Database): Объект базы данных, по умолчанию используется 'db'.

        Returns:
            None
        """
    database.connect()
    database.create_tables([Query, Response])
