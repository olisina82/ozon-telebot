from loader import bot
import tg_API.handlers  # noqa
from tg_API.utils.set_bot_commands import set_default_commands
from database.utils.db_create import create_database
from logs.utils.logs_creation import write_log_error


if __name__ == "__main__":
    """
        Основная функция для запуска бота.

        Устанавливает стандартные команды бота, создает базу данных и запускает бота в бесконечном режиме.

        Returns:
            None
    """
    try:
        set_default_commands(bot)
        create_database()
        bot.infinity_polling(none_stop=True)
    except Exception as exc:
        error_message = f'Ошибка при запуске ТГ {exc}'
        write_log_error(error_message)

