from site_API.utils.product_api import get_product_info
from logs.utils.logs_creation import log_error_decorator

@log_error_decorator('Ошибка при выполнении custom data processing')
def custom_price(min_price: int, max_price: int) -> list:
    """
        Выполняет обработку данных о продуктах в заданном ценовом диапазоне.

        Args:
            min_price (int): Минимальная цена для фильтрации продуктов.
            max_price (int): Максимальная цена для фильтрации продуктов.

        Returns:
            List[Tuple[float, str, str]]: Список кортежей с данными о продуктах в указанном ценовом диапазоне.
                Каждый кортеж содержит цену, название и ссылку на изображение.

        """
    all_product_list = get_product_info()
    custom_items = sorted([info for info in all_product_list if min_price <= info[0] <= max_price])
    return custom_items

