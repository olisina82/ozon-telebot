from site_API.utils.product_api import get_product_info
from logs.utils.logs_creation import log_error_decorator


@log_error_decorator('Ошибка при выполнении high data processing')
def maximum_price() -> list:
    """
        Выполняет обработку данных о продуктах и возвращает список продуктов в порядке убывания.

        Returns:
            List[Tuple[float, str, str]]: Список кортежей с данными о продуктах с максимальной ценой.
                Каждый кортеж содержит цену, название и ссылку на изображение.
        """
    all_product_list = get_product_info()
    sorted_items = sorted(all_product_list, key=lambda info: info[0], reverse=True)
    return sorted_items
