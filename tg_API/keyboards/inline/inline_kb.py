from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton


def show_menu_inline() -> InlineKeyboardMarkup:
    """
        Генерирует инлайн-клавиатуру для отображения меню команд.

        Returns:
            InlineKeyboardMarkup: Объект с кнопками меню.
        """
    keyboard = InlineKeyboardMarkup(row_width=1)
    callback_button1 = InlineKeyboardButton(text="📌 Растения с минимальной ценой", callback_data="/low")
    callback_button2 = InlineKeyboardButton(text="📌 Растения с максимальной ценой", callback_data="/high")
    callback_button3 = InlineKeyboardButton(text="📌 Найти растения в диапазоне цен", callback_data="/custom")
    callback_button4 = InlineKeyboardButton(text="📡 Показать историю запросов", callback_data="/history")
    callback_button5 = InlineKeyboardButton(text="🏃 Перейти на сайт",
                                            url='https://www.ozon.ru/seller/sactus-bazar-1298613/products/?miniapp=seller_1298613')
    keyboard.add(callback_button1, callback_button2, callback_button3, callback_button4, callback_button5)
    return keyboard


def cancel() -> InlineKeyboardMarkup:
    """
    Генерирует инлайн-клавиатуру для отмены операции.

    Returns:
        InlineKeyboardMarkup: Объект с кнопкой отмены.
    """
    keyboard2 = InlineKeyboardMarkup()
    callback_button = InlineKeyboardButton(text="Отмена", callback_data="/cancel")
    keyboard2.add(callback_button)
    return keyboard2


