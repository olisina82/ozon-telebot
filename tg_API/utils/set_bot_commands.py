from telebot.types import BotCommand
from telebot import TeleBot
from config_data.config import DEFAULT_COMMANDS


def set_default_commands(bot: TeleBot):
    """
       Устанавливает список стандартных команд бота.

       Args:
           bot (TeleBot): Объект бота, к которому применяются команды.

       Returns:
           None
       """
    bot.set_my_commands(
        [BotCommand(*i) for i in DEFAULT_COMMANDS]
    )
