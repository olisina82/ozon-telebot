from telebot.types import Message
from tg_API.keyboards.inline.inline_kb import show_menu_inline
from loader import bot
from logs.utils.logs_creation import log_error_decorator


@log_error_decorator('шибка ТГ бота: обработка команды Echo')
@bot.message_handler(state=None, content_types=['text', 'photo', 'video'])
def bot_echo(message: Message):
    """
        Обрабатывает сообщения от пользователя и отправляет ответ с предложением воспользоваться меню.

        Args:
            message (Message): Объект, представляющий сообщение от пользователя.

        Returns:
            None
        """
    bot.reply_to(
        message,
        "Мне нужен от Вас запрос, я еще маленький и не умею разговаривать на свободные темы 😪",
        reply_markup=show_menu_inline()
    )

