from telebot.types import Message
from config_data.config import DEFAULT_COMMANDS
from loader import bot
from tg_API.keyboards.inline.inline_kb import show_menu_inline
from logs.utils.logs_creation import log_error_decorator


@log_error_decorator('Ошибка ТГ бота: обработка команды Help')
@bot.message_handler(commands=["help"])
def bot_help(message: Message):
    """
            Обрабатывает команду /help и отправляет пользователю список доступных команд.

            Args:
                message (Message): Объект, представляющий сообщение от пользователя.

            Returns:
                None
            """
    text = [f"/{command} - {desk}" for command, desk in DEFAULT_COMMANDS]
    bot.reply_to(message, "\n".join(text), reply_markup=show_menu_inline())
