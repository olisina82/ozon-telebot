from tg_API.keyboards.inline.inline_kb import show_menu_inline
from telebot.types import Message
from loader import bot
from logs.utils.logs_creation import log_error_decorator


@log_error_decorator('Ошибка ТГ бота: обработка команды Start')
@bot.message_handler(commands=["start"])
def bot_start(message: Message):
    """
        Обрабатывает команду /start и отправляет приветственное сообщение и меню пользователю.

        Args:
            message (Message): Объект, представляющий сообщение от пользователя.

        Returns:
            None
        """
    bot.reply_to(message, f"Привет, {message.from_user.full_name}! 👋\n\n"
                          f"Я бот Ozon магазина *Cuctus-bazar* 🌵\n\n"
                          f"Если тебе нужна будет помощь, введи /help или выбери эту команду из меню\n\n"
                          f"Я могу показать тебе:\n"
                          f"    • товары с минимальной ценой\n"
                          f"    • товары с максимальной ценой\n"
                          f"    • товары в назначенном тобой диапазоне цен\n"
                          f"    • историю твоих последних 10 запросов\n"
                          f"Или можем перейти на сайт\n\n"
                          f"Просто нажми на нужный пункт и я тебе все покажу 😁👇",
                 reply_markup=show_menu_inline(), parse_mode='Markdown')


