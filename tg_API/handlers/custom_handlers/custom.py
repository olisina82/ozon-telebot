from tg_API.data_processing import custom_data_processing, low_data_processing
from loader import bot
from telebot import custom_filters
from telebot.types import Message, CallbackQuery
from tg_API.states.states import MyStates
from tg_API.keyboards.inline.inline_kb import show_menu_inline, cancel
from database.CRUD.crud_operations import create_query_response
from logs.utils.logs_creation import log_error_decorator

bot.add_custom_filter(custom_filters.StateFilter(bot))
bot.add_custom_filter(custom_filters.IsDigitFilter())


@log_error_decorator('Ошибка ТГ бота: обработка команды Custom1')
@bot.callback_query_handler(func=lambda call: call.data == '/custom')
def custom_command_callback(call: CallbackQuery):
    """
       Обрабатывает команду /custom и инициирует процесс ввода пользовательских данных.

       Args:
           call (CallbackQuery): Объект, представляющий запрос пользователя.

       Returns:
           None
       """
    message = call.message
    chat_id = message.chat.id
    bot.send_message(chat_id, "Сколько позиций вывести?", reply_markup=cancel())
    bot.set_state(call.from_user.id, MyStates.custom_awaiting_number, chat_id)


@log_error_decorator('Ошибка ТГ бота: обработка команды Custom2')
@bot.message_handler(state=[MyStates.custom_awaiting_number, MyStates.custom_min_cost, MyStates.custom_max_cost],
                     regexp=r'\b0\b|\D')
def number_incorrect(message: Message):
    """
    Обрабатывает случаи, когда пользователь вводит некорректное число.

    Args:
        message (Message): Объект, представляющий сообщение от пользователя.

    Returns:
        None
    """
    bot.send_message(message.chat.id, "Пожалуйста, введите корректное число.", reply_markup=cancel())


@log_error_decorator('Ошибка ТГ бота: обработка команды Custom3')
@bot.message_handler(state=MyStates.custom_awaiting_number, is_digit=True)
def minimum_cost(message: Message):
    """
        Обрабатывает ввод пользователем количества позици и инициирует процесс ввода пользовательских данных.

        Args:
            message (Message): Объект, представляющий сообщение от пользователя.

        Returns:
            None
        """
    with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
        data['output_qty'] = message.text
    sorted_list = low_data_processing.minimum_price()
    min_price, max_price = sorted_list[0][0], sorted_list[-1][0]
    bot.send_message(message.chat.id, f"В наличии растения в диапазоне цен\nот {min_price} руб. до {max_price} руб.\n"
                                      f"Начиная с какой цены смотрим?", reply_markup=cancel())
    bot.set_state(message.from_user.id, MyStates.custom_min_cost, message.chat.id)


@log_error_decorator('Ошибка ТГ бота: обработка команды Custom4')
@bot.message_handler(state=MyStates.custom_min_cost, is_digit=True)
def max_cost(message: Message):
    """
        Обрабатывает ввод пользователем минимальной цены и инициирует процесс ввода пользовательских данных.

        Args:
            message (Message): Объект, представляющий сообщение от пользователя.

        Returns:
            None
        """
    with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
        data['min_cost'] = message.text
    bot.send_message(message.chat.id, "До какой цены смотрим?", reply_markup=cancel())
    bot.set_state(message.from_user.id, MyStates.custom_max_cost, message.chat.id)


@log_error_decorator('Ошибка ТГ бота: обработка команды Custom5')
@bot.message_handler(state=MyStates.custom_max_cost, is_digit=True)
def process_custom_command(message: Message):
    """
       Обрабатывает пользовательские данные и выводит результат.

       Args:
           message (Message): Объект, представляющий сообщение от пользователя.

       Returns:
           None
       """
    with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
        output_qty = int(data['output_qty'])
        minimum_cost = int(data['min_cost'])
    maximum_cost = int(message.text)
    items = custom_data_processing.custom_price(minimum_cost, maximum_cost)
    response_messages = []
    bot.send_message(message.chat.id, '*Растения в выбранном диапазоне цен:*', parse_mode='Markdown')
    for item in items[:output_qty]:
        price, name, primary_image = item
        response_message = f"*Цена*: {price} руб.\n*Имя товара*: {name}"
        bot.send_photo(message.chat.id, primary_image, caption=response_message, parse_mode='Markdown')
        response_messages.append(item)
    if len(response_messages) < 1:
        msg = 'Нет товаров в таком диапазоне цен'
        response_messages.append(msg)
        bot.send_message(message.chat.id, msg, reply_markup=show_menu_inline())

    query_text = "Команда /custom"
    query, response = create_query_response(message.from_user.id, query_text, response_messages)
    bot.delete_state(message.from_user.id, message.chat.id)
    bot.send_message(message.chat.id, '\n\n⚙️ Выбери пункт меню:', reply_markup=show_menu_inline())
