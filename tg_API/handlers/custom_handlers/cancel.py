from loader import bot
from tg_API.keyboards.inline.inline_kb import show_menu_inline
from logs.utils.logs_creation import log_error_decorator
from telebot.types import CallbackQuery


@log_error_decorator('Ошибка ТГ бота: обработка команды Cancel')
@bot.callback_query_handler(func=lambda call: call.data == '/cancel')
def any_state(call: CallbackQuery):
    """
        Обрабатывает команду отмены (/cancel) в Телеграм боте.

        Args:
            call (CallbackQuery): Объект, представляющий запрос пользователя.

        Returns:
            None
        """
    bot.delete_state(call.from_user.id, call.message.chat.id)
    bot.answer_callback_query(callback_query_id=call.id, show_alert=False, text="Отмена ❌")
    bot.send_message(call.message.chat.id, '\n\n⚙️ Выбери пункт меню:', reply_markup=show_menu_inline())


