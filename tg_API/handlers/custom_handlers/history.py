from loader import bot
from tg_API.keyboards.inline.inline_kb import show_menu_inline
from database.CRUD.crud_operations import read_query_responses
from ast import literal_eval
from logs.utils.logs_creation import log_error_decorator
from telebot.types import CallbackQuery
from telebot import util


@log_error_decorator('Ошибка ТГ бота: обработка команды History')
@bot.callback_query_handler(func=lambda call: call.data == '/history')
def history_command_callback(call: CallbackQuery):
    """
       Обрабатывает команду /history и выводит историю запросов и ответов пользователя.

       Args:
           call (CallbackQuery): Объект, представляющий запрос пользователя.

       Returns:
           None
       """
    message = call.message
    chat_id = message.chat.id
    last_records = read_query_responses()
    common_message = ''

    for number, query in enumerate(last_records):
        query_message = f'*Запрос №{number+1}*\n{query.created_at}\n*Запрос:* {query.query_text}\n\n'
        common_message += query_message
        for response in query.responses:
            response_list = literal_eval(response.response_text)
            for item in response_list:
                if len(item) == 3:
                    price, name, image_url = item
                    response_message = f"*Название товара*: {name}\n*Цена*: {price} руб.\n\n"
                else:
                    response_message = f'{item}\n\n'
                common_message += response_message
        common_message += 90 * '-' + '\n'
    for i_part in util.smart_split(common_message, 3000):
        bot.send_message(chat_id, i_part, parse_mode='Markdown')
    bot.send_message(message.chat.id, '\n\n⚙️ Выбери пункт меню:', reply_markup=show_menu_inline())

