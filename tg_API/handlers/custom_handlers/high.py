from tg_API.data_processing import high_data_processing
from loader import bot
from telebot import custom_filters
from telebot.types import Message, CallbackQuery
from tg_API.states.states import MyStates
from tg_API.keyboards.inline.inline_kb import show_menu_inline, cancel
from database.CRUD.crud_operations import create_query_response
from logs.utils.logs_creation import log_error_decorator

bot.add_custom_filter(custom_filters.StateFilter(bot))
bot.add_custom_filter(custom_filters.IsDigitFilter())


@log_error_decorator('Ошибка ТГ бота: обработка команды High1')
@bot.callback_query_handler(func=lambda call: call.data == '/high')
def high_command_callback(call: CallbackQuery):
    """
        Обрабатывает команду /high и инициирует процесс ввода пользовательских данных.

        Args:
            call (CallbackQuery): Объект, представляющий запрос пользователя.

        Returns:
            None
        """
    message = call.message
    chat_id = message.chat.id
    bot.send_message(chat_id, "Сколько позиций показать?", reply_markup=cancel())
    bot.set_state(call.from_user.id, MyStates.high_awaiting_number, chat_id)


@log_error_decorator('Ошибка ТГ бота: обработка команды High2')
@bot.message_handler(state=MyStates.high_awaiting_number, regexp=r'\b0\b|\D')
def number_incorrect(message: Message):
    """
       Обрабатывает случаи, когда пользователь вводит некорректное число.

       Args:
           message (Message): Объект, представляющий сообщение от пользователя.

       Returns:
           None
       """
    bot.send_message(message.chat.id, "Пожалуйста, введите корректное число.", reply_markup=cancel())


@log_error_decorator('Ошибка ТГ бота: обработка команды High3')
@bot.message_handler(state=MyStates.high_awaiting_number, is_digit=True)
def process_low_command(message: Message):
    """
        Обрабатывает пользовательские данные и выводит результат.

        Args:
            message (Message): Объект, представляющий сообщение от пользователя.

        Returns:
            None
        """
    output_qty = int(message.text)
    items = high_data_processing.maximum_price()
    response_messages = []
    bot.reply_to(message, '*Растения с максимальной ценой:*', parse_mode='Markdown')
    for item in items[:output_qty]:
        max_price, max_name, max_primary_image = item
        response_message = f"*Цена*: {max_price} руб.\n*Имя товара*: {max_name}"
        bot.send_photo(message.chat.id, max_primary_image, caption=response_message, parse_mode='Markdown')
        response_messages.append(item)

    query_text = "Команда /high"
    query, response = create_query_response(message.from_user.id, query_text, response_messages)
    bot.delete_state(message.from_user.id, message.chat.id)
    bot.send_message(message.chat.id, '\n\n⚙️ Выбери пункт меню:', reply_markup=show_menu_inline())