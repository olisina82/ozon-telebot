from telebot.handler_backends import State, StatesGroup


class MyStates(StatesGroup):
    """
        Группа состояний для бота.

        Attributes:
            low_awaiting_number (State): Состояние ожидания ввода количества товаров с минимальной ценой.
            high_awaiting_number (State): Состояние ожидания ввода количества товаров с максимальной ценой.
            custom_awaiting_number (State): Состояние ожидания ввода количества товаров с пользовательскими ценами.
            custom_min_cost (State): Состояние ожидания ввода минимальной цены для пользовательского запроса.
            custom_max_cost (State): Состояние ожидания ввода максимальной цены для пользовательского запроса.
        """
    low_awaiting_number = State()
    high_awaiting_number = State()
    custom_awaiting_number = State()
    custom_min_cost = State()
    custom_max_cost = State()


