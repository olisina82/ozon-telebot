import requests
import json
from site_API.auth import header_api
from logs.utils.logs_creation import write_log_error
import re
from typing import List, Tuple


def _get_product_list() -> List[int]:
    """
        Получает список продуктов с помощью API и возвращает список идентификаторов продуктов.

        Returns:
            List[int]: Список идентификаторов продуктов.
        """
    product_list_url = "https://api-seller.ozon.ru/v2/product/list"
    request_product_body = {
        "filter": {
            "offer_id": [],
            "product_id": [],
            "visibility": "ALL"
        },
        "last_id": "",
        "limit": 1000
    }
    try:
        response_product = requests.post(product_list_url, headers=header_api(), json=request_product_body)
        response_product.raise_for_status()
        product_data = json.loads(response_product.text)
        product_list = [i['product_id'] for i in product_data['result']['items'] if i.get('is_fbs_visible') is True]
        return product_list
    except requests.exceptions.RequestException as exc:
        exc_record = f'Ошибка при выполнении запроса получения списка продуктов: {exc}'
        write_log_error(exc_record)
        return []


def get_product_info() -> List[Tuple[float, str, str]]:
    """
       Получает информацию о продуктах с помощью API и возвращает список кортежей с данными о продуктах.

       Returns:
           List[Tuple[float, str, str]]: Список кортежей с данными о продуктах. Каждый кортеж содержит цену, название и ссылку на изображение.
       """
    price_url = "https://api-seller.ozon.ru/v2/product/info/list"
    product_list = _get_product_list()
    if not product_list:
        return []

    request_body = {
        'offer_id': [],
        'product_id': product_list,
        'sku': [],
    }
    try:
        response = requests.post(price_url, headers=header_api(), json=request_body)
        response.raise_for_status()
        price_data = json.loads(response.text)
        pattern = r'^[^,]*'
        items = price_data['result']['items']
        item_info = list(
            map(lambda item: (float(item['price'].rstrip('00')), re.match(pattern, item['name']).group(), item['primary_image']), items))
        return item_info
    except requests.exceptions.RequestException as exc:
        exc_record = f'Ошибка при выполнении запроса получения информации о продуктах: {exc}'
        write_log_error(exc_record)
        return []
