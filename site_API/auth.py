import os
from logs.utils.logs_creation import write_log_error
from typing import Dict

def header_api() -> Dict[str, str]:
    """
        Создает и возвращает заголовки для API-запросов.

        Заголовки включают в себя 'Client-Id' и 'Api-Key', полученные из переменных среды.

        Returns:
            Dict[str, str]: Словарь с заголовками для API-запросов.
        Raises:
            ValueError: Если 'Client-Id' или 'Api-Key' не найдены в переменных среды.
        """
    client_id = os.getenv('SHOP_CLIENT_ID')
    api_key = os.getenv('SHOP_API_KEY')
    if not client_id or not api_key:
        mistake = "Ошибка подключения к магазину: Client-Id или/и Api-Key не найдены в переменных среды"
        write_log_error(mistake)
        raise ValueError("Client-Id или/и Api-Key не найдены в переменных среды")

    headers = {
        'Client-Id': client_id,
        'Api-Key': api_key,
    }
    return headers

client_id = os.getenv('SHOP_CLIENT_ID')


